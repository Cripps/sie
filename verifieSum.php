<?php
function dropDead($message) {
    echo $message.PHP_EOL;
    die;
}

if (! isset($argv[1])) dropDead("Missing argv...");
$filename = $argv[1];
if (! file_exists($filename)) dropDead("File dont exist: ".$filename);

$sieObj = new verifieSum($filename);

class verifieSum
{

    private $dataVerifications = [];
    private $currentVerification;
    private $verificationErrors = [];

    private function stripLineBreak($string)
    {
        return str_replace(["\r","\n"],"",$string);
    }

    private function getLineName($fullString)
    {
        $lastWordStart = strripos($fullString,'"', -3)+1;
        return substr($fullString,$lastWordStart,strripos($fullString,'"')-$lastWordStart);
    }

    private function getFloat($value)
    {
        return preg_replace("~[^\p{N}-.]++~u", '', $value);
    }

    private function startVerification(string $verString)
    {
        $verName = $this->getLineName($verString);
        $this->currentVerification = [
            'name' => $verName,
            'accounts' => [],
            'sum' => 0,
        ];
    }

    private function addTransaction(string $transString)
    {
        $account = $this->getFloat(strstr($transString, "{", true));
        $sum = $this->getFloat(strstr($transString,"}"));
        if(isset($this->currentVerification['accounts'][$account])) {
            $this->currentVerification['accounts'][$account] += $sum;
        } else {
            $this->currentVerification['accounts'][$account] = $sum;
        }
        $this->currentVerification['sum'] += $sum;
    }

    private function endVerification()
    {
        $this->dataVerifications[] = $this->currentVerification;
        $allFine = true;
        if(round($this->currentVerification['sum'],2) != 0) $allFine = false;
        if(! $allFine) $this->verificationErrors[] = $this->currentVerification;
        $this->currentVerification = null;
    }

    private function finalizeVerifiactions()
    {
        if(count($this->verificationErrors) > 0) {
            foreach ($this->verificationErrors AS $key => $verError) {
                if($key > 0) echo PHP_EOL;
                var_dump($verError); echo PHP_EOL;
                echo "Name: ".$verError['name'].PHP_EOL;
                echo "Sum: ".$verError['sum'].PHP_EOL;
            }
        } else {
            echo "File is fine";
        }
        echo PHP_EOL;
    }

    public function __construct(string $filename = null)
    {
        if(! is_null($filename)) $this->start($filename);
    }

    public function start(string $filename)
    {
        $filedata = file($filename);
        foreach ($filedata as $filerow) {
            $filerow = $this->stripLineBreak($filerow);
            $rowType = substr($filerow, 0, 4);
            switch ($rowType) {
                case "#VER":
                    $this->startVerification($filerow);
                    break;
                case "#TRA":
                    // #TRANS 3041 {} -540 "" "Kontorsstäd timpris"
                    $this->addTransaction($filerow);
                    break;
                case "}":
                    $this->endVerification();
                    break;
                default:
                    // echo $rowType.PHP_EOL;
            }
        }
        $this->finalizeVerifiactions();
    }
}
// echo "Rows: ".count($filedata);


